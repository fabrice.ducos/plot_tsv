#!/usr/bin/env python3

import os

def parseBoolean(booleanString):
    booleanLower = booleanString.lower()
    if (booleanLower in ('false', 'no', '0', 'off')):
        return False
    elif (booleanLower in ('true', 'yes', '1', 'on')):
        return True
    else:
        raise ValueError('%s: not a valid boolean string ({false|no|off|0} or {true|yes|on|1} expected)' % booleanString)

def convert_name_to_aliased_variable(name, alias = None):
    if (not isinstance(name, str)):
        raise TypeError('name should be a string')
    if (alias is None): alias = os.path.basename(name)
    return { alias: name }

def convert_names_to_aliased_variables(names):
    if isinstance(names, str):
        name = names
        variables = convert_name_to_aliased_variable(name)
    elif isinstance(names, (list, tuple)):
        variables = dict()
        for name in names:
            variable = convert_name_to_aliased_variable(name)
            variables.update(variable)
    elif isinstance(names, dict):
        variables = names
    else:
        raise TypeError("the `names' argument should be a string, a list, a tuple or a dict, got a " + str(type(names)))

    return variables

def cm2inch(*tupl):
    # credit: https://stackoverflow.com/questions/14708695/specify-figure-size-in-centimeter-in-matplotlib
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)
