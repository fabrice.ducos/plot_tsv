#!/usr/bin/env python3

# Fabrice Ducos 2019, 2020

import sys
import csv
import json
import math
import yaml
import optparse
import os
import subprocess
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.basemap import Basemap
from types import SimpleNamespace
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import warnings

from scipy.interpolate.interpnd import _ndim_coords_from_arrays
from scipy.spatial import cKDTree

# non standard modules
from apiutils import cm2inch
from DotDict import DotDict

# Global Variables
verbose = True
debug = True
appname = os.path.basename(__file__)

# Functions

def read_stream(stream):
    for line in stream:
        print(line,)

    stream.close()

def add_options(option_parser):
    option_parser.add_option('--verbose', '-v', action='store_true', dest='verbose', default=False, help="sets verbose mode")
    option_parser.add_option('--input-sep', '-s', action=None, dest='input_separator', default=',', help="sets the input separator (comma by default, use '\\t' for tabulation)")
    option_parser.add_option('--plot-kind', '-K', action=None, dest='plot_kind', default='scatter', help="selects the kind of plot (scatter or mesh, default is scatter)")
    option_parser.add_option('--output',  '-o', action=None, dest='output_filename', default=None,  help="sets output filename")
    option_parser.add_option('--title', '-T', action=None, dest='title', default=None, help="sets a title (will override the title in the .yml file if present)")

def usage(option_parser):
    return option_parser.usage()

def read_yaml(filename):
    # reference: https://stackoverflow.com/questions/1773805/how-can-i-parse-a-yaml-file-in-python
    with open(filename) as stream:
        data = yaml.safe_load(stream)
    return data

def read_config(filename, dot_syntax = False):
    config = read_yaml(filename)

    if (dot_syntax == True):
        #return SimpleNamespace(**config)
        return DotDict(config)
    else:
        return config

def print_log(message, stream = sys.stderr):
    now = datetime.datetime.now(tz=None)
    print(message + " at " + str(now), file = stream)

def print_info(message, stream = sys.stdout):
    print(appname + ':', message, file = stream)

def print_error(message):
    print_info(message, stream = sys.stderr)

def get_map_extent(config):
    if ('map_extent' in config):
        lat_min = float(config.map_extent.lat_min)
        lat_max = float(config.map_extent.lat_max)
        lat_step = float(config.map_extent.lat_step)
        lon_min = float(config.map_extent.lon_min)
        lon_max = float(config.map_extent.lon_max)
        lon_step = float(config.map_extent.lon_step)
    else:
        lat_min, lat_max, lat_step = -90, 90, 30
        lon_min, lon_max, lon_step = -180, 180, 30

    return lat_min, lat_max, lat_step, lon_min, lon_max, lon_step
    
def init_plot(config):
    fill_color = config.fill_color
    continent_color = config.continent_color
    lake_color = config.lake_color
    
    lat_min, lat_max, lat_step, lon_min, lon_max, lon_step = get_map_extent(config)
    parallels = np.arange(lat_min, lat_max, lat_step)
    meridians = np.arange(lon_min, lon_max, lon_step)

    m = Basemap(**config.projection_settings)

    m.drawcoastlines()
    #m.drawmapboundary(fill_color = fill_color)
    #m.fillcontinents(color = continent_color,lake_color = lake_color)
    m.drawparallels(parallels, labels = [ True,  False, False, False ]) # left, right, top, bottom
    m.drawmeridians(meridians, labels = [ False, False, False,  True ]) # left, right, top, bottom
    
    return m
    
def plot_scatter(data, config):
    # https://matplotlib.org/basemap/users/examples.html

    m = init_plot(config)
    
    title = config.title
    if (config.options.title is not None):
        title = config.options.title
    
    colormap = plt.get_cmap(config.colorscale.colormap) # reference: https://matplotlib.org/tutorials/colors/colormaps.html

    # set minimum and maximum to None for automatic scaling
    minimum = None if (config.colorscale.min == 'auto') else float(config.colorscale.min)
    maximum = None if (config.colorscale.max == 'auto') else float(config.colorscale.max)

    alpha = float(config.scatter.alpha)
    marker = config.scatter.marker
    
    m.scatter(data.longitudes, data.latitudes, marker = marker, c = data.values, vmin = minimum, vmax = maximum, 
              cmap = colormap, zorder = 0, alpha = alpha)

    # forces the size of colorbar to be adjusted to the main graph
    ax = plt.gca()
    ax.set_aspect('auto')
    plt.colorbar(ax = ax)
    
    plt.title(title)

    if (config.options.output_filename is None):
        plt.show()
    else:
        plt.savefig(config.options.output_filename)

def plot_mesh(data, config):
    from scipy.interpolate import griddata
    
    m = init_plot(config)

    title = config.title
    if (config.options.title is not None):
        title = config.options.title

    colormap = plt.get_cmap(config.colorscale.colormap) # reference: https://matplotlib.org/tutorials/colors/colormaps.html

    # set minimum and maximum to None for automatic scaling
    minimum = None if (config.colorscale.min == 'auto') else float(config.colorscale.min)
    maximum = None if (config.colorscale.max == 'auto') else float(config.colorscale.max)

    alpha = float(config.scatter.alpha)
    lat_min, lat_max, lat_step, lon_min, lon_max, lon_step = get_map_extent(config)
    warnings.warn('lat_res and lon_res currently hardcoded')
    lat_res = 1
    lon_res = 1

    # reference: https://stackoverflow.com/questions/45273202/make-an-python-plot-from-recurring-arrays/45273661#45273661
    data_points = (data.longitudes, data.latitudes)
    longitudes = np.arange(lon_min, lon_max, lon_res)
    latitudes  = np.arange(lat_min, lat_max, lat_res)
    grd_lon, grd_lat = np.meshgrid(longitudes, latitudes)
    grd_points = (grd_lon, grd_lat)
    interp_data = griddata(data_points, data.values, grd_points, method = 'linear') # method = 'linear', 'nearest', 'cubic'
    
    # https://stackoverflow.com/questions/30655749/how-to-set-a-maximum-distance-between-points-for-interpolation-when-using-scipy
    #tree = cKDTree(data_points)
    #tree = cKDTree(interp_data)
    #distances, indices = tree.query(interp_data)
    #threshold = 1.0
    #print(distances)
    #interp_data[distances > threshold] = np.nan
    
    # reference: https://matplotlib.org/basemap/api/basemap_api.html#mpl_toolkits.basemap.Basemap.pcolor
    # https://matplotlib.org/api/_as_gen/matplotlib.pyplot.pcolor.html
    m.pcolormesh(grd_lon, grd_lat, interp_data, latlon = True, vmin = minimum, vmax = maximum,
                 cmap = colormap, zorder = 0, alpha = alpha)

    # forces the size of colorbar to be adjusted to the main graph
    ax = plt.gca()
    ax.set_aspect('auto')
    plt.colorbar(ax = ax)
    
    plt.title(title)
    
    if (config.options.output_filename is None):
        plt.show()
    else:
        plt.savefig(config.options.output_filename)

        
def plot(data, config):
    plot_kind = config.options.plot_kind
    if (plot_kind == 'scatter'):
        plot_scatter(data, config)
    elif (plot_kind == 'mesh'):
        plot_mesh(data, config)
    else:
        print("%s: wrong argument for -K|--plot-kind: %s (should be 'scatter' or 'mesh')" % (appname, config.options.plot_kind), file = sys.stderr)
        sys.exit(1)
        
def get_stem(filename):
    stem = os.path.basename(filename)
    stem = os.path.splitext(stem)[0]
    return stem

def get_column_indices(header_row, latitude_label, longitude_label, parameter_label):
    ilat = header_row.index(latitude_label)
    ilon = header_row.index(longitude_label)
    ivalue = header_row.index(parameter_label)
    return ilat, ilon, ivalue

def get_data(input_stream, config):
    csv_reader = csv.reader(input_stream, delimiter = config.options.input_separator)
    header_row = next(csv_reader)[0].split()

    latitudes = list()
    longitudes = list()
    values = list()
    
    chosen_latitude = config.variables.latitude
    chosen_longitude = config.variables.longitude
    chosen_level = config.variables.level
    ilat, ilon, ivalue = get_column_indices(header_row, chosen_latitude, chosen_longitude, chosen_level)
    
    lat_min = config.map_extent.lat_min
    lat_max = config.map_extent.lat_max
    lon_min = config.map_extent.lon_min
    lon_max = config.map_extent.lon_max
    value_min = -float('inf') # -math.inf (available in 3.5) # config.colorscale.min
    value_max = float('inf') # math.inf (available in 3.5)  # config.colorscale.max

    for row in csv_reader:
        fields = row[0].split()
        lat = float(fields[ilat])
        lon = float(fields[ilon])
        value = float(fields[ivalue])
        
        if (not (lat_min <= lat <= lat_max)): continue
        if (not (lon_min <= lon <= lon_max)): continue
        if (not (value_min <= value <= value_max)): continue

        latitudes.append(lat)
        longitudes.append(lon)
        values.append(value)

    data_dict = dict()
    data_dict['latitudes'] = np.array(latitudes)
    data_dict['longitudes'] = np.array(longitudes)
    data_dict['values'] = np.array(values)
    data = SimpleNamespace(**data_dict)
    
    return data

def main():
    option_parser = optparse.OptionParser(
        usage='%prog plot_cfg.yml [input_file.tsv]',
        description='plots geolocated data from a .tsv file, according to the specification given in plot_cfg.yml')
    add_options(option_parser)
    (options, args) = option_parser.parse_args()

    if (options.input_separator == '\\t'):
        options.input_separator = '\t'
                     
    if (len(args) != 1 and len(args) != 2):
        option_parser.error('incorrect number of arguments')
    
    config_file = args[0]

    try:
        config = read_config(config_file, dot_syntax = True)
        config.options = options
    except FileNotFoundError as e:
        print_error(e)
        sys.exit(1)

    if (len(args) == 1):
        data = get_data(sys.stdin, config)
        plot(data, config)
    else:
        input_file = args[1]
        with open(input_file) as input_stream:
            data = get_data(input_stream, config)
            plot(data, config)

    sys.exit(0)
    
if __name__ == '__main__':
    main()

    
